import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-app';

  selectedAnimal: {
    name: string,
    img: string,
    weight: string,
    speed: string,
  } = {
    name: 'Pangolin',
    img: '/assets/pangolin.jpg',
    weight: '3.6 - 33 kg',
    speed: ''
    };

  animalsArray: Object[] = [
    {name: 'Pangolin', img: '/assets/pangolin.jpg', weight: '3.6 - 33 kg', speed: '15 km/h'},
    {name: 'Chauve-souris', img: '/assets/bat.jpg', weight: '1.1 kg', speed: '50 km/h'},
    {name: 'Fennec', img: '/assets/fennec.jpg', weight: '0,68 – 1,6 kg', speed: '?? km/h'},
    {name: 'Pingouin', img: '/assets/pingouin.jpg', weight: '10 - 25 kg', speed: '6 - 9 km/h'},
  ];

  selectAnimal(item){
    this.selectedAnimal.name=item.name;
    this.selectedAnimal.img=item.img;
    this.selectedAnimal.weight=item.weight;
    this.selectedAnimal.speed=item.speed;
  }
}
